var express = require('express');
var router = express.Router();

/**
 * BD
 */
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
    database: 'biblicalreading'
});

/* GET users listing. */
router.get('/', function (req, res, next) {
    //res.send('respond with a resource');
    connection.query('select * from comentarios', function (error, result) {
        res.render("../views/comments/index", { comments: result });
    });
});

module.exports = router;